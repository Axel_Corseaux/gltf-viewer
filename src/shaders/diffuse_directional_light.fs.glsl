#version 330

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;

out vec3 fColor;

uniform vec3 uLightingDirection;
uniform vec3 uLightingIntensity;

#define PI 3.1415926538f

vec3 simpleBRDF() {
   return vec3(1.f / PI, 1.f / PI, 1.f / PI); 
}

vec3 getRadiance() {
   vec3 normalizedViewSpaceNormal = normalize(vViewSpaceNormal);
   return simpleBRDF() * uLightingIntensity * dot(normalizedViewSpaceNormal, uLightingDirection);
}

void main()
{
   fColor = getRadiance();
}